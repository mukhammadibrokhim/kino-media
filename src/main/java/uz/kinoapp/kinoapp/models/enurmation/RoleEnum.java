package uz.kinoapp.kinoapp.models.enurmation;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_MODERATOR
}
