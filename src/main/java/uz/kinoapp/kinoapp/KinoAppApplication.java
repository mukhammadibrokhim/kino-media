package uz.kinoapp.kinoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KinoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(KinoAppApplication.class, args);
    }

}
