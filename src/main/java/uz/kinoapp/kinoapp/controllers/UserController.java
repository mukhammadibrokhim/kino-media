package uz.kinoapp.kinoapp.controllers;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import uz.kinoapp.kinoapp.models.User;
import uz.kinoapp.kinoapp.services.UserServiceImpl;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class UserController {

    private final UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String getRegistration(User user, Model model) {
        model.addAttribute("user", user);
        return "registration";
    }

    @PostMapping("/registration")
    public String postRegistration(
            User user,
            Model model,
            BindingResult result,
            HttpServletRequest request
    ) throws MessagingException, UnsupportedEncodingException {
        if (!userService.checkPassword(user.getPassword())) {
            result.rejectValue("password", "password", "Password length must be more than 6");
        }
        if (userService.existEmail(user)) {
            result.rejectValue("email", "email", "This email has already been used");
        }
        if (!user.getPassword().equals(user.getConfirm())) {
            result.rejectValue("confirm", "confirm", "Passwords are different");
        }
        if (userService.existUserName(user)) {
            result.rejectValue("username", "username", "This username has already been used");
        }
        if (result.hasErrors()) {
            return "registration";
        }
        String siteUrl = userService.getSiteUrl(request);
        model.addAttribute("user", user);

        userService.registry(user, siteUrl);
        return "redirect:/registration/success";
    }

    @GetMapping("/registration/success")
    public String getSuccess(){
        return "messages/registration_success";
    }

    @GetMapping("/verify")
    public String verifyUser(@Param("code") String code) {
        if (userService.verify(code)) {
            return "messages/verify_success";
        } else {
            return "messages/verify_fail";
        }
    }

}
