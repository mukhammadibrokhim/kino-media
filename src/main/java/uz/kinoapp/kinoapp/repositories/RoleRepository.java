package uz.kinoapp.kinoapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.kinoapp.kinoapp.models.Roles;
import uz.kinoapp.kinoapp.models.enurmation.RoleEnum;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Roles, Long> {
    Optional<Roles> findByName(RoleEnum roleEnum);
}
