package uz.kinoapp.kinoapp.services;

import net.bytebuddy.utility.RandomString;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import uz.kinoapp.kinoapp.models.User;
import uz.kinoapp.kinoapp.models.enurmation.Provider;
import uz.kinoapp.kinoapp.repositories.UserRepository;
import uz.kinoapp.kinoapp.services.interfaces.UserService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JavaMailSender mailSender;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, JavaMailSender mailSender) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.mailSender = mailSender;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User registry(User user, String siteUrl) throws MessagingException, UnsupportedEncodingException {
        String randomCode = RandomString.make(64);
        user.setVerificationCode(randomCode);
        user.setEnabled(false);
        sendVerificationEmail(user, siteUrl);
        user.setProvider(Provider.LOCAL);
        user.setEnabled(Boolean.FALSE);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User setBlock(User user) {
        user.setEnabled(Boolean.FALSE);
        return null;
    }

    public Boolean checkPassword(String password) {
        return password.length() >= 6;
    }

    public Boolean existUserName(User user) {
        return userRepository.existsByUsername(user.getUsername());
    }

    public Boolean existEmail(User user) {
        return userRepository.existsByEmail(user.getEmail());
    }

    private void sendVerificationEmail(User user, String siteURL)
            throws MessagingException, UnsupportedEncodingException {
        String toAddress = user.getEmail();
        String fromAddress = "kinomediamain@gmail.com";
        String senderName = "Kino-Media";
        String subject = "Please verify your registration";
        String verifyUrl = siteURL + "/verify?code=" + user.getVerificationCode();
        String content = "Dear " + user.getFullName() + ",<br>"
                + "Please click the link below to verify your registration:<br>"
                + "<h3><a href=\"" + verifyUrl + "\">VERIFY</a></h3>"
                + "Thank you,<br>"
                + "Mukhammadibrokhim.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", user.getFullName());
        String verifyURL = siteURL + "/verify?code=" + user.getVerificationCode();

        content = content.replace("[[URL]]", verifyURL);

        helper.setText(content, true);

        mailSender.send(message);

    }

    public boolean verify(String verificationCode) {
        User user = userRepository.findByVerificationCode(verificationCode);

        if (user == null || user.getEnabled()) {
            return false;
        } else {
            user.setVerificationCode(null);
            user.setEnabled(true);
            userRepository.save(user);
            return true;
        }
    }

    public static String getSiteUrl(HttpServletRequest request) {
        String siteUrl = request.getRequestURL().toString();
        return siteUrl.replace(request.getServletPath(), "");
    }

    public void processOAuthPostLogin(String username,String firstname,String lastName) {
        User existUser = userRepository.findByUsername(username);
        if (existUser == null) {
            User newUser = new User();
            newUser.setUsername(username);
            newUser.setName(firstname);
            newUser.setSurname(lastName);
            newUser.setProvider(Provider.GOOGLE);
            newUser.setEnabled(true);

            userRepository.save(newUser);
        }

    }

    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }
}
