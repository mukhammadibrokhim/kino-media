package uz.kinoapp.kinoapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.kinoapp.kinoapp.models.User;
import uz.kinoapp.kinoapp.models.enurmation.Provider;
import uz.kinoapp.kinoapp.repositories.UserRepository;
import uz.kinoapp.kinoapp.security.CustomOAuth2User;

@Service
public class UserService {

    @Autowired
    private UserRepository repo;

    public void processOAuthPostLogin(String username,String firstname,String lastName) {
        User existUser = repo.findByUsername(username);
        if (existUser == null) {
            User newUser = new User();
            newUser.setUsername(username);
            newUser.setName(firstname);
            newUser.setSurname(lastName);
            newUser.setProvider(Provider.GOOGLE);
            newUser.setEnabled(true);

            repo.save(newUser);
        }

    }
}
