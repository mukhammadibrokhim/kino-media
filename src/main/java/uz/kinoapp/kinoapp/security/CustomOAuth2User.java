package uz.kinoapp.kinoapp.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.Map;

public class CustomOAuth2User implements OAuth2User {
    private final OAuth2User oAuth2User;

    public CustomOAuth2User(OAuth2User oAuth2User) {
        this.oAuth2User = oAuth2User;
    }


    @Override
    public Map<String, Object> getAttributes() {
        return oAuth2User.getAttributes();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return oAuth2User.getAuthorities();
    }

    @Override
    public String getName() {
        return oAuth2User.getName();
    }

    public String givenName(){
        return oAuth2User.<String> getAttribute("given_name");
    }

    public String familyName(){
        return oAuth2User.<String> getAttribute("family_name");
    }

    public String getEmail() {
        return oAuth2User.<String>getAttribute("email");
    }

    public OAuth2User getoAuth2User() {
        return oAuth2User;
    }
}
