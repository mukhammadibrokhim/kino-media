//package uz.kinoapp.kinoapp.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import uz.kinoapp.kinoapp.security.CustomOAuth2User;
//import uz.kinoapp.kinoapp.security.CustomOAuth2UserService;
//import uz.kinoapp.kinoapp.services.UserService;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@Configuration
//@EnableWebSecurity
//public class Config extends WebSecurityConfigurerAdapter {
//    private final UserService userService;
//
//    public Config(UserService userService) {
//        this.userService = userService;
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .csrf().disable()
//                .headers().disable()
//                .authorizeRequests()
//                .antMatchers("/").permitAll()
//                .antMatchers("/download/**").authenticated()
////                .and()
////                .oauth2Login()
////                    .loginPage("/login")
////                    .userInfoEndpoint()
////                    .userService(oauthUserService)
////                .and()
////                    .successHandler(new AuthenticationSuccessHandler() {
////
////                        @Override
////                        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
////                                                            Authentication authentication) throws IOException, ServletException {
////
////                            CustomOAuth2User oauthUser = (CustomOAuth2User) authentication.getPrincipal();
////
////                            userService.processOAuthPostLogin(oauthUser.getEmail(),oauthUser.givenName(),oauthUser.familyName());
////
////                            response.sendRedirect("/");
////                        }
////                    })
////                .and()
//                .httpBasic();
//    }
//
//
////    @Override
////    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////        auth
////                .inMemoryAuthentication().passwordEncoder(passwordEncoder());
////    }
//
//
//    @Autowired
//    private CustomOAuth2UserService oauthUserService;
//
//
////    @Bean
////    public PasswordEncoder passwordEncoder() {
////        return new BCryptPasswordEncoder();
////    }
//
//}
