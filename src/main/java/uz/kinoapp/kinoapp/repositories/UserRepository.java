package uz.kinoapp.kinoapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.kinoapp.kinoapp.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

//    User findAllByEnabled();

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User getByEmail(String  email);

    User findByVerificationCode(String verificationCode);
}
