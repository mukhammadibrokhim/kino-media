package uz.kinoapp.kinoapp.services.interfaces;

import org.springframework.validation.BindingResult;
import uz.kinoapp.kinoapp.models.User;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface UserService {
    User save(User user);

    User registry(User user, String siteUrl) throws MessagingException, UnsupportedEncodingException;

    User setBlock(User user);
}
