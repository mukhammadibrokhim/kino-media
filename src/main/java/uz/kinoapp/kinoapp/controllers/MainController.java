package uz.kinoapp.kinoapp.controllers;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import uz.kinoapp.kinoapp.repositories.UserRepository;
import uz.kinoapp.kinoapp.security.CustomUserDetailService;
import uz.kinoapp.kinoapp.services.UserServiceImpl;

@Controller
public class MainController {

    private final CustomUserDetailService customUserDetailService;
    private final UserRepository userRepository;

    public MainController(UserServiceImpl userService, UserRepository userRepository, CustomUserDetailService customUserDetailService) {
        this.customUserDetailService = customUserDetailService;
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String getIndex() {
        return "index";
    }

    @GetMapping(value = "/login")
    public String getLogin() {
        return "login";
    }

    @GetMapping("/logout")
    public String getLogout() {
        SecurityContextHolder.clearContext();
        return "redirect:/";
    }

    @GetMapping("/admin")
    public String getAdminPage() {
        return "admin/admin_panel";
    }


}
