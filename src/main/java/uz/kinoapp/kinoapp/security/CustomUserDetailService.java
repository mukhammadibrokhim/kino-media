package uz.kinoapp.kinoapp.security;

import org.omg.CORBA.UserException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import uz.kinoapp.kinoapp.models.User;
import uz.kinoapp.kinoapp.repositories.UserRepository;
import uz.kinoapp.kinoapp.services.UserServiceImpl;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class CustomUserDetailService implements UserDetailsService {
    private final UserServiceImpl userService;

    public CustomUserDetailService(UserServiceImpl userService) {
        this.userService = userService;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new UserDetails() {
            User user = userService.findByUsername(username);

            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return user.getRoles().stream().map(roles -> new SimpleGrantedAuthority(roles.getName().name())).collect(Collectors.toList());
            }

            @Override
            public String getPassword() {
                return user.getPassword();
            }

            @Override
            public String getUsername() {
                return user.getUsername();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return  user.getEnabled();
            }
        };
    }
}
